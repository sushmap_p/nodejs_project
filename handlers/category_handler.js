var Category = require("../models/category");
var Product = require("../models/product");

module.exports = {

    add_category: function (data, callback) {
        categorydata = function () {

            Category.findOne({
                "category_id": data.category_id
            }, function (err, categorydata) {
                if (err) {
                    callback({
                        statuscode: 500,
                        error: err,
                        msg: "finding category_id database error"
                    });
                } else if (categorydata != null) {
                    callback({
                        statuscode: 304,
                        msg: "category already exist"
                    });

                } else {
                    var addcategory = new Category();
                    addcategory.category_id = data.category_id;
                    addcategory.category_name = data.category_name;
                    addcategory.save(function(err,result){
                        if(err){
                            callback({
                                statuscode: 500,
                                error: err,
                                msg: "saving category, database error"
                            });
                        }else{
                            callback({
                                statuscode: 200,
                                msg: "category added successfully",
                                data: addcategory
                            });
                        }
                    });
                    
                }
            });
        }
        process.nextTick(categorydata);
    },
      get_categoryproduct_count: function (callback) {
        categorydata = async function () {
            // var collection = new Category();
           await Category.aggregate([
                {
                    $lookup:
                    {
                        from: 'products',
                        localField: "category_id",
                        foreignField: "category_id",
                        as: 'abcroduct'
                    }
                },
                {
                    $project:
                    {
                        category_id: 1,
                        category_name: 1,
                        totalProducts: {$size: "$abcroduct"},
                        // number_of_product: '$abcroduct'
                    }
                }
            ], function(err, result) {
                if (err) throw err;
                 callback({
                                statuscode: 200,
                                msg: "products counts listed successfully",
                                data: result
                            });
            });
        }
        process.nextTick(categorydata);
    }
}


