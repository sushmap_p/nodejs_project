var Product = require("../models/product");

module.exports = {

    add_product: function (data, callback) {
        productdata = function () {
console.log(data);

            Product.findOne({
                "product_id": data.product_id
            }, function (err, productdata) {
                if (err) {
                    callback({
                        statuscode: 500,
                        error: err,
                        msg: "finding product_id database error"
                    });
                } else if (productdata != null) {
                    callback({
                        statuscode: 304,
                        msg: "product already exist"
                    });

                } else {
                    var addproduct = new Product();
                    addproduct.product_id = data.product_id;
                    addproduct.product_name = data.product_name;
                    addproduct.category_id = data.category_id;
                    console.log(addproduct);
                    
                    addproduct.save(function(err,result){
                        if(err){
                            callback({
                                statuscode: 500,
                                error: err,
                                msg: "saving product, database error"
                            });
                        }else{
                            callback({
                                statuscode: 200,
                                msg: "product added successfully",
                                data: addproduct
                            });
                        }
                    });
                    
                }
            });
        }
        process.nextTick(productdata);
    }
}


