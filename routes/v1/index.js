var bCrypt = require('bcrypt-nodejs');
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
var BASE_API_URL = "";
var version = "1.0"; // version code
/* congig */
var configuration = require("../../config");
/* utils*/

/* handler */
var CategoryHandler = require("../../handlers/category_handler");
var ProductHandler = require("../../handlers/product_handler");



module.exports = function (passport) {

router.post(BASE_API_URL + '/add_category', function (req, res) {
var data = req.body;
CategoryHandler.add_category(data,function (response) {
response.version = version;
res.json(response);
});
});

router.post(BASE_API_URL + '/add_product', function (req, res) {
    var data = req.body;
    ProductHandler.add_product(data,function (response) {
    response.version = version;
    res.json(response);
    });
    });

    router.get(BASE_API_URL + '/get_category_product_count', function (req, res) {
        // var data = req.body;
        CategoryHandler.get_categoryproduct_count(function (response) {
        response.version = version;
        res.json(response);
        });
        });
    

    

   



    return router;
};
