var mongoose = require('mongoose');
module.exports = mongoose.model('product', {
   product_name:String,
   product_id: Number,
   category_id:Number
});