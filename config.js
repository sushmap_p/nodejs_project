module.exports = {
    "APPLICATION_PORT": 9000,
    "TOKEN_SECRET": "!@project_name@!",
    "TOKEN_EXPIRY": 525600,
    "ALERT_EMAIL": "abc@gmail.com",
    "VERSION_V1_URL": '/project_name/api/v1/',
    "SMS_CAMPAIGN_NAME": "project_name",
    "SMS_SENDER_NAME": "project_name",
    "SENDER_EMAIL": "",
    "EMAIL_PASSWORD": "",
    "GMAIL_PASSWORD":""
}
