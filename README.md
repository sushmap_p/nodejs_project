## Requirements
- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

If the installation was successful, you should be able to run the following command.

    $ node --version
    v8.11.3

    $ npm --version
    6.1.0

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g


## Install

    $ git clone https://github.com/YOUR_USERNAME/PROJECT_TITLE
    $ cd PROJECT_TITLE
    $ npm install


## Running the project

    $ npm start



## about the APIs included 
I have included add category and add product API in index.js file with the names add_category and add_product.
I have included category and product count in index.js with the name get_category_product_count

in the postman/browser you need to access the api with the URL http://192.168.0.167:9000/project_name/api/v1/get_category_product_count

you will get the proper response .